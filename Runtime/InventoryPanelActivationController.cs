using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace com.overlordgamestudio.inventorysystem
{
    public class InventoryPanelActivationController : MonoBehaviour
    {
        [SerializeField] private CanvasGroup cGroup;
        [SerializeField] private float actionSpeed;
        [SerializeField] private bool disableObjectOnHide;

        private bool showing;
        private bool hiding;

        private UnityAction showInventoryDelegate;
        private UnityAction hideInventoryDelegate;

        private void Start()
        {
            if (disableObjectOnHide)
            {
                cGroup.gameObject.SetActive(false);
                cGroup.alpha = 0;
            }
        }
        public void RegisterShowInventoryCallback(UnityAction a)
        {
            if (showInventoryDelegate == null)
            {
                showInventoryDelegate = new UnityAction(a);
            }
            else
            {
                showInventoryDelegate += a;
            }
        }
        public void RegisterHideInventoryCallback(UnityAction a)
        {
            if (hideInventoryDelegate == null)
            {
                hideInventoryDelegate = new UnityAction(a);
            }
            else
            {
                hideInventoryDelegate += a;
            }
        }
        public void Show()
        {
            if (!hiding)
            {
                StartCoroutine(ShowCR());
            }
        }

        public void Hide()
        {
            if (!showing)
            {
                StartCoroutine(HideCR());
            }
        }


        private IEnumerator ShowCR() 
        {
            showing = true;

            if (disableObjectOnHide) 
            {
                cGroup.gameObject.SetActive(true);
            }

            while (cGroup.alpha < 1) 
            {
                cGroup.alpha += Time.deltaTime * actionSpeed;
                yield return null;
            }

            if (showInventoryDelegate != null) 
            {
                showInventoryDelegate.Invoke();
            }

            showing = false;
        }

        private IEnumerator HideCR()
        {
            hiding = true;

            while (cGroup.alpha > 0)
            {
                cGroup.alpha -= Time.deltaTime * actionSpeed;
                yield return null;
            }

            if (hideInventoryDelegate != null)
            {
                hideInventoryDelegate.Invoke();
            }

            if (disableObjectOnHide)
            {
                cGroup.gameObject.SetActive(false);
            }

            hiding = false;
        }
    }
}
