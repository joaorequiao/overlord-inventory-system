using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace com.overlordgamestudio.inventorysystem
{
    public class InventorySlot : MonoBehaviour
    {
        [SerializeField] private Image selectedImage;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI amountText;
        [SerializeField] private string amountPrefix;
        private InventoryItem _item;
        
        private int _amount;
        
        public InventoryItem Item => _item;

        

        public void SetItem(InventoryItem item)
        {
            _item = item;
            icon.enabled = true;
            icon.sprite = _item.icon;
            _amount = item.amount;
            amountText.text = amountPrefix + item.amount.ToString();
            amountText.gameObject.SetActive(true);
        }

        public void UpdateAmount(int newAmount)
        {
            _amount = newAmount;

            if (_amount <= 0)
            {
                Clear();
            }
            else
            {
                amountText.text = amountPrefix + _amount.ToString();
                amountText.gameObject.SetActive(true);
            }
        }

        public void Clear()
        {
            _item = null;
            icon.enabled = false;
            _amount = 0;
            amountText.gameObject.SetActive(false);
            Deselect();
        }

        public void Select()
        {
            selectedImage.enabled = true;
        }

        public void Deselect()
        {
            selectedImage.enabled = false;
        }
    }
}
