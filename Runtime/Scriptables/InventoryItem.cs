using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.overlordgamestudio.inventorysystem
{
    [CreateAssetMenu(fileName = "_Item", menuName = "Overlord Game Studio/Inventory System/Item")]
    public class InventoryItem : ScriptableObject
    {
        public string itemID;
        public Sprite icon;

        public string itemName;
        public string description;
        public int amount;
    }
}
