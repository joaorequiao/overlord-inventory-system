using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace com.overlordgamestudio.inventorysystem
{
    [CreateAssetMenu(fileName = "_Inventory", menuName = "Overlord Game Studio/Inventory System/Inventory")]
    public class InventoryList : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<InventoryItem> items;
        public List<InventoryItem> Items => items;
        
        public int maxSlots = 4;

        private bool _changed;

        public bool HasChanges => _changed;
        
        private UnityAction reloardInventoryDelegate;

        public void SetReloadCallback(UnityAction a)
        {
            if (reloardInventoryDelegate == null)
            {
                reloardInventoryDelegate = new UnityAction(a);
                return;
            }

            reloardInventoryDelegate += a;
        }
        
        public void CommitChanges()
        {
            _changed = false;
        }

        public void AddItem(InventoryItem item, int amount)
        {
            InventoryItem i = GetItemById(item.itemID);

            if (i == null)
            {
                InventoryItem toAdd = Instantiate(item);
                toAdd.amount = amount;
                items.Add(toAdd);
            }
            else
            {
                i.amount += amount;
            }

            _changed = true;
            if(reloardInventoryDelegate is null) return;
            reloardInventoryDelegate.Invoke();
        }

        public void RemoveItem(string id, int amount)
        {
            InventoryItem i = GetItemById(id);

            if (i == null) return;
            
            i.amount -= amount;

            if (i.amount <= 0)
            {
                items.Remove(i);
            }
            
            _changed = true;
            if(reloardInventoryDelegate is null) return;
            reloardInventoryDelegate.Invoke();
        }

        public InventoryItem GetItemById(string id)
        {
            return items.FirstOrDefault(i => i.itemID == id);
        }

        public void AddSlot()
        {
            maxSlots++;
            _changed = true;
            if(reloardInventoryDelegate is null) return;
            reloardInventoryDelegate.Invoke();
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            items.Clear();
        }
    }
}
