using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.overlordgamestudio.OverlordToolbox.Input;

namespace com.overlordgamestudio.inventorysystem
{
    public class InventoryNavigator : MonoBehaviour
    {
        [SerializeField] private InventoryInfoPanel infoPanel;
        [SerializeField] private Transform itemsPanel;
        [SerializeField] private GameObject slotPrefab;
        [SerializeField] private InventoryList inventory;
        [SerializeField] private bool hasMoveTimer;
        [SerializeField] private float moveTimer = 0.4f;
        
        private List<InventorySlot> _slots;
        private int _selectedSlot;
        private bool _canMoveLeft;
        private bool _canMoveRight;
        private float _timerElapsed;
        
        private void Start()
        {
            inventory.SetReloadCallback(SetupInventoryViewer);
            
            _slots = new List<InventorySlot>();

            while (_slots.Count < inventory.maxSlots)
            {
                AddSlot();
            }
            
            SetupInventoryViewer();
        }

        private void OnEnable()
        {
            if (_slots != null)
            {
                SetupInventoryViewer();
            }
        }

        public void EnableInventoryNavigation() 
        {
            OverlordInputManager.EnableMenusModeMaps();
        }

        public void DisableInventoryNavigation() 
        {
            OverlordInputManager.EnableGameModeMaps();
        }

        private void SetupInventoryViewer()
        {
            if (inventory.HasChanges)
            {
                while (_slots.Count < inventory.maxSlots)
                {
                    AddSlot();
                }
            }

            foreach (InventorySlot s in _slots)
            {
                s.Clear();
            }

            for (int i = 0; i < inventory.Items.Count; i++)
            {
                _slots[i].SetItem(inventory.Items[i]);
            }

            _selectedSlot = 0;
            SelectSlot(_selectedSlot);
            inventory.CommitChanges();            
        }

        public void AddSlot()
        {
            InventorySlot s = Instantiate(slotPrefab, itemsPanel).GetComponent<InventorySlot>();
            s.Clear();
            _slots.Add(s);
        }

        private void Update()
        {
            Navigate();
        }

        private void Navigate()
        {
            if (_canMoveLeft && OverlordInputManager.HorizontalMenu() > OverlordInputManager.inputThreshold_Mid)
            {
                _canMoveLeft = false;
                _timerElapsed = 0;
                
                _slots[_selectedSlot].Deselect();
                _selectedSlot++;
                if (_selectedSlot >= _slots.Count)
                {
                    _selectedSlot = 0;
                }
                
                SelectSlot(_selectedSlot);
            }

            if (_canMoveRight && OverlordInputManager.HorizontalMenu() < -OverlordInputManager.inputThreshold_Mid)
            {
                _canMoveRight = false;
                _timerElapsed = 0;
                
                _slots[_selectedSlot].Deselect();
                _selectedSlot--;
                if (_selectedSlot < 0)
                {
                    _selectedSlot = _slots.Count - 1;
                }

                SelectSlot(_selectedSlot);
            }

            if (!_canMoveLeft && OverlordInputManager.HorizontalMenu() < OverlordInputManager.inputThreshold_Mid)
            {
                _canMoveLeft = true;
            }

            if (!_canMoveRight && OverlordInputManager.HorizontalMenu() > -OverlordInputManager.inputThreshold_Mid)
            {
                _canMoveRight = true;
            }

            if (hasMoveTimer)
            {
                if (!_canMoveLeft || !_canMoveRight)
                {
                    _timerElapsed += Time.unscaledDeltaTime;

                    if (_timerElapsed >= moveTimer)
                    {
                        _canMoveLeft = true;
                        _canMoveRight = true;
                    }
                }
            }
        }

        private void SelectSlot(int index)
        {
            if (_slots.Count <= index) return;
            _slots[index].Select();
            infoPanel.SetInfo(_slots[index].Item);
        }

    }
}
