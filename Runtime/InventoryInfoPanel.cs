using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace com.overlordgamestudio.inventorysystem
{
    public class InventoryInfoPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI name;
        [SerializeField] private TextMeshProUGUI description;

        private void Start()
        {
            name.text = "";
            description.text = "";
        }

        public void SetInfo(InventoryItem item)
        {
            if (item is null)
            {
                name.text = "";
                description.text = "";
            }
            else
            {
                name.text = item.itemName;
                description.text = item.description;
            }
        }
    }
}
